import React, { Component } from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css';
import { Router, Switch, Route } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import OAuth from './components/OAuth';
import GenerateToken from './components/GenerateToken';
import ContestsList from './components/ContestsList';
import Leaderboard from './components/Leaderboard';
import RefreshToken from './components/RefreshToken';

const customHistory = createBrowserHistory();

window.localStorage.baseURL = 'https://api.codechef.com';
window.localStorage.clientId = '6126ebab0d4cd651dd6bad20b9181b7f';
window.localStorage.clientSecret = 'f580fae4e6b37ef08b7a96600d42bac3';
//for production
window.localStorage.redirect_uri = 'http://149.129.139.192:5000/generate_token';
//for local
// window.localStorage.redirect_uri = 'http://localhost:3000/generate_token';

class App extends Component {
  render() {
    return (
      <div className="padding-sm">
        <Router history={customHistory}>
          <Switch>
            <Route exact path="/" component={OAuth} />
            <Route path="/generate_token" component={GenerateToken} />
            <Route exact path="/leaderboard" component={ContestsList} />
            <Route path="/leaderboard/:contestCode" component={Leaderboard} />
            <Route path="/error_page" component={RefreshToken} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
