export const getCodeFromUrl = url => {
  try {
    const splittedByEqualSign = url.split('=');
    return splittedByEqualSign[1].split('&')[0];
  } catch (e) {
    console.error(e);
    return null;
  }
};
