import moment from 'moment';
const dateFormat = 'YYYY-MM-DD HH:mm:ss';

export function renderDateDiff(startDate, endDate, durationFormat = 'hours') {
  if (!endDate || !startDate) return null;
  return moment
    .unix(endDate, dateFormat)
    .diff(moment.unix(startDate, dateFormat), durationFormat);
}

export function renderHumanReadableDate(date) {
  return moment(date, dateFormat).format('MMMM Do YYYY, h:mm a');
}

export function renderRelativeTime(date) {
  return moment(date, dateFormat).fromNow();
}
