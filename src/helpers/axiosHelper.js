import axios from 'axios';

export function axiosCall(props) {
  const { action, url, payload, params, timeout, headers } = props;
  return axios({
    method: action,
    url: url,
    data: payload,
    params: params || {},
    timeout: timeout || 30000,
    headers: headers || {}
  })
    .then(response => ({ response }))
    .catch(error => ({ error: error.response }));
}

export function axiosCallWithAccessTokenGranted(props) {
  return axios({
    ...props,
    headers: {
      common: { Authorization: `Bearer ${window.localStorage.access_token}` }
    }
  })
    .then(response => ({ response }))
    .catch(error => ({ error: error.response }));
}
