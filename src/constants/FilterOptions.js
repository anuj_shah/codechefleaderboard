export const SORT_CONTESTS_FIELDS = [
  { key: 0, text: 'Contest code', value: 'code' },
  { key: 1, text: 'Contest name', value: 'name' },
  { key: 2, text: 'Start date', value: 'startDate' },
  { key: 3, text: 'End date', value: 'endDate' }
];
