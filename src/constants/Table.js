export const TABLE_FIELDS = [
  'country',
  'rank',
  'username',
  'total_problems_solved',
  'totalScore',
  'totalTime'
];
