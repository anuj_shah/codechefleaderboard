import React, { Component } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { axiosCallWithAccessTokenGranted } from '../helpers/axiosHelper';
import SegmentLoader from './SegmentLoader';
import RankTable from './RankTable';
import { Pagination } from 'semantic-ui-react';
import CountrySearch from './CountrySearch';

const PAGE_LIMIT = 1500;

class Leaderboard extends Component {
  state = { loading: false, activePage: 1 };

  componentDidMount() {
    this.setRankLists({ offset: 0, limit: PAGE_LIMIT });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.country !== this.state.country && this.state.country) {
      const { activePage, country } = this.state;
      this.setRankLists({
        offset: (activePage - 1) * PAGE_LIMIT,
        limit: PAGE_LIMIT,
        country
      });
    }
  }

  setRankLists = async params => {
    this.setState({ loading: true });
    try {
      const { response, error } = await axiosCallWithAccessTokenGranted({
        action: 'GET',
        url: `${window.localStorage.baseURL}/rankings/${get(
          this.props,
          'match.params.contestCode'
        )}`,
        params
      });

      this.setState({ loading: false });
      if (response) {
        this.setState({
          rankList: get(response, 'data.result.data.content')
        });
      } else if (get(error, 'status') === 401) {
        this.props.history.push('/error_page');
      } else this.props.history.replace('/');
    } catch (e) {
      this.setState({ loading: false });
      this.props.history.replace('/');
      console.error(e);
    }
  };

  handlePageChange = (e, data) => {
    const { activePage } = data;
    this.setState({ activePage });
    this.setRankLists({
      offset: (activePage - 1) * PAGE_LIMIT,
      limit: PAGE_LIMIT,
      country: this.state.country
    });
  };

  onClick = (e, data) => {
    this.setState({ country: data.value });
  };

  render() {
    if (this.state.loading)
      return <SegmentLoader textMessage="Fetching Ranklist" />;
    return (
      <React.Fragment>
        <div style={{ height: '5vh', width: '15vw', marginBottom: '10px' }}>
          <CountrySearch onClick={this.onClick} />
        </div>
        <div style={{ height: '85vh', overflowY: 'scroll' }}>
          <RankTable dataSource={this.state.rankList} />
        </div>
        <div style={{ marginTop: '5px' }}>
          <Pagination
            onPageChange={this.handlePageChange}
            activePage={this.state.activePage}
            totalPages={10}
          />
        </div>
      </React.Fragment>
    );
  }
}

Leaderboard.propTypes = {};

export default Leaderboard;
