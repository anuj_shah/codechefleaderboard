import React, { Component } from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import debounce from 'lodash/debounce';
import { Dropdown } from 'semantic-ui-react';
import { axiosCallWithAccessTokenGranted } from '../helpers/axiosHelper';

class CountrySearch extends Component {
  constructor(props) {
    super(props);
    this.state = { countryOptions: [], loading: false };
    this.onCountrySearch = debounce(this.onCountrySearch, 800);
  }

  setCountries = async params => {
    this.setState({ loading: true });
    try {
      const { response, error } = await axiosCallWithAccessTokenGranted({
        action: 'GET',
        url: `${window.localStorage.baseURL}/country`,
        params
      });
      this.setState({ loading: false });
      if (response) {
        const countryList = get(response, 'data.result.data.content');
        if (countryList && countryList.length)
          this.setState({
            countryOptions: this.getCountryOptions(countryList)
          });
      } else if (get(error, 'status') === 401) {
        this.props.history.push('/error_page');
      } else this.props.history.replace('/');
    } catch (e) {
      console.error(e);
      this.setState({ loading: false });
      this.props.history.replace('/');
    }
  };

  getCountryOptions = countries =>
    countries.map(country => {
      const { countryCode, countryName } = country;
      return {
        key: countryCode,
        value: countryName,
        flag: countryCode.toLowerCase(),
        text: countryName
      };
    });

  onCountrySearch = (e, data) => {
    const { searchQuery } = data;
    if (searchQuery) this.setCountries({ search: searchQuery });
  };

  render() {
    return (
      <div>
        <Dropdown
          selectOnNavigation={false}
          loading={this.state.loading}
          placeholder="Select country"
          fluid
          search
          selection
          onSearchChange={this.onCountrySearch}
          options={this.state.countryOptions}
          onChange={this.props.onClick}
        />
      </div>
    );
  }
}

CountrySearch.propTypes = {
  onClick: PropTypes.func
};

export default withRouter(CountrySearch);
