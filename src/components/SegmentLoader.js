import React, { Component } from 'react';
import { Message, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

class SegmentLoader extends Component {
  render() {
    return (
      <Message icon>
        <Icon name="circle notched" loading />
        <Message.Content>
          <Message.Header>Just one second</Message.Header>
          {this.props.textMessage}
        </Message.Content>
      </Message>
    );
  }
}

SegmentLoader.defaultProps = {
  textMessage: 'We are fetching that content for you.'
};

SegmentLoader.propTypes = {
  textMessage: PropTypes.string
};

export default SegmentLoader;
