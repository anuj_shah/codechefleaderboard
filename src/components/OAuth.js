import React, { Component } from 'react';
import { Button, Image } from 'semantic-ui-react';
import codechefLogo from '../logo/codechef-leaderboard-logo.PNG';
import PropTypes from 'prop-types';

class OAuth extends Component {
  render() {
    const { redirect_uri, clientId, baseURL } = window.localStorage;
    return (
      <div>
        <Image src={codechefLogo} size="large" centered />
        <div style={{ textAlign: 'center', marginTop: '10px' }}>
          <a
            href={`${baseURL}/oauth/authorize?response_type=code&client_id=${clientId}&state=xyz&redirect_uri=${redirect_uri}`}
          >
            <Button primary>Click to authorize</Button>
          </a>
        </div>
      </div>
    );
  }
}

OAuth.propTypes = {};

export default OAuth;
