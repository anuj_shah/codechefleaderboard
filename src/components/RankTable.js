import React, { Component } from 'react';
import { Table, Message, Flag } from 'semantic-ui-react';
import startCase from 'lodash/startCase';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import { TABLE_FIELDS } from '../constants/Table';

class RankTable extends Component {
  renderHeaders = () => (
    <Table.Row>
      {TABLE_FIELDS.map(field => (
        <Table.HeaderCell>{startCase(field)}</Table.HeaderCell>
      ))}
    </Table.Row>
  );

  renderBody = () =>
    this.props.dataSource.map(data => (
      <Table.Row>
        {TABLE_FIELDS.map(field => {
          if (field === 'total_problems_solved') {
            return (
              <Table.Cell>{get(data, 'problemScore', []).length}</Table.Cell>
            );
          }
          if (field === 'country') {
            const countryName = get(data, field, '');
            return (
              <Table.Cell>
                <span>
                  <Flag name={countryName.toLowerCase()} />
                </span>
                &nbsp;
                <span>{countryName}</span>
              </Table.Cell>
            );
          }
          return <Table.Cell>{get(data, field)}</Table.Cell>;
        })}
      </Table.Row>
    ));

  renderErrorMessage = () => (
    <Message negative>
      <Message.Header>
        We're sorry the data you were looking for was not found
      </Message.Header>
      <p>Maybe the leaderboard for this contest has not been updated!!!</p>
    </Message>
  );

  render() {
    if (!this.props.dataSource.length) return this.renderErrorMessage();
    return (
      <Table celled>
        <Table.Header>{this.renderHeaders()}</Table.Header>
        <Table.Body>{this.renderBody()}</Table.Body>
      </Table>
    );
  }
}

RankTable.defaultProps = {
  dataSource: []
};

RankTable.propTypes = {
  dataSource: PropTypes.array
};

export default RankTable;
