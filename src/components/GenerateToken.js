import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { axiosCall } from '../helpers/axiosHelper';
import get from 'lodash/get';
import { getCodeFromUrl } from '../helpers/getCodeFromUrl';
import SegmentLoader from './SegmentLoader';


class GenerateToken extends Component {
  async componentDidMount() {
    this.setAxiosTokenHeader();
  }

  setAxiosTokenHeader = async () => {
    const {
      baseURL,
      clientId,
      clientSecret,
      redirect_uri
    } = window.localStorage;
    try {
      const { response, error } = await axiosCall({
        action: 'POST',
        url: `${baseURL}/oauth/token`,
        payload: {
          grant_type: 'authorization_code',
          code: getCodeFromUrl(this.props.location.search),
          client_id: clientId,
          client_secret: clientSecret,
          redirect_uri
        }
      });
      if (response) {
        const data = get(response, 'data.result.data');
        window.localStorage.access_token = get(data, 'access_token');
        window.localStorage.refresh_token = get(data, 'refresh_token');
      } else if (get(error, 'status') === 401) {
        this.props.history.push('/error_page');
      } else this.props.history.replace('/');
      this.props.history.push('/leaderboard');
    } catch (e) {
      this.props.history.replace('/');
      console.error(e);
    }
  };

  render() {
    return (
      <div>
        <SegmentLoader textMessage="Generating Token" />
      </div>
    );
  }
}

GenerateToken.propTypes = {};

export default GenerateToken;
