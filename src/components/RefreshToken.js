import React, { Component } from 'react';
import { Message, Button } from 'semantic-ui-react';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import { axiosCall } from '../helpers/axiosHelper';
import SegmentLoader from './SegmentLoader';

class RefreshToken extends Component {
  state = { loading: false };
  handleTokenRegenerate = async () => {
    const {
      clientId,
      clientSecret,
      refresh_token,
      baseURL
    } = window.localStorage;
    try {
      this.setState({ loading: true });
      const { response, error } = await axiosCall({
        action: 'POST',
        url: `${baseURL}/oauth/token`,
        payload: {
          grant_type: 'refresh_token',
          refresh_token,
          client_id: clientId,
          client_secret: clientSecret
        }
      });
      if (response) {
        const data = get(response, 'data.result.data');
        window.localStorage.access_token = get(data, 'access_token');
        window.localStorage.refresh_token = get(data, 'refresh_token');
      } else if (get(error, 'status') === 401) {
        this.props.history.push('/error_page');
      } else this.props.history.replace('/');
      this.props.history.goBack();
    } catch (e) {
      this.setState({ loading: false });
      this.props.history.replace('/');
      console.error(e);
    }
  };

  render() {
    if (this.state.loading)
      return <SegmentLoader textMessage="Regenerating token" />;
    return (
      <div>
        <Message>
          <Message.Header>Authorization failed</Message.Header>
          <p>Authorization failed, please click below to regenerate token</p>
        </Message>
        <Button onClick={this.handleTokenRegenerate} secondary>
          Regnerate token
        </Button>
      </div>
    );
  }
}

RefreshToken.propTypes = {};

export default RefreshToken;
