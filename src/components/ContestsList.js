import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { axiosCallWithAccessTokenGranted } from '../helpers/axiosHelper';
import { Card, Dropdown, Form, Radio } from 'semantic-ui-react';
import get from 'lodash/get';
import {
  renderHumanReadableDate,
  renderRelativeTime
} from '../helpers/renderHelpers';
import SegmentLoader from './SegmentLoader';
import { CARD_COLORS } from '../constants/CardColors';
import { getRandomInt } from '../helpers/randomInt';
import { SORT_CONTESTS_FIELDS } from '../constants/FilterOptions';

class ContestsList extends Component {
  state = {
    contestList: [],
    loading: false,
    sortField: 'startDate',
    sortOrder: 'desc'
  };
  componentDidMount() {
    this.setContests({ limit: 100 });
  }

  componentDidUpdate(prevProps, prevState) {
    const { sortField, sortOrder } = this.state;
    if (
      prevState.sortField !== sortField ||
      prevState.sortOrder !== sortOrder
    ) {
      this.setContests({ limit: 100, sortBy: sortField, sortOrder });
    }
  }

  setContests = async params => {
    this.setState({ loading: true });
    try {
      const { response, error } = await axiosCallWithAccessTokenGranted({
        action: 'GET',
        url: `${window.localStorage.baseURL}/contests`,
        params
      });
      this.setState({ loading: false });
      if (response) {
        this.setState({
          contestList: this.getContestAsCardsInformation(
            get(response, 'data.result.data.content.contestList')
          )
        });
      } else if (get(error, 'status') === 401) {
        this.props.history.push('/error_page');
      } else this.props.history.replace('/');
    } catch (e) {
      this.setState({ loading: false });
      this.props.history.replace('/');
      console.error(e);
    }
  };

  getContestAsCardsInformation = contests => {
    return contests.map(contest => {
      return {
        header: get(contest, 'code'),
        description: get(contest, 'name'),
        meta: (
          <div>
            <div>
              <b>Start: </b>
              {renderHumanReadableDate(get(contest, 'startDate'))}
            </div>
            <div>
              <b>Time: </b>
              {renderRelativeTime(get(contest, 'startDate'))}
            </div>
          </div>
        )
      };
    });
  };

  handleChange = ({ value, fieldType }) => {
    this.setState({ [fieldType]: value });
  };

  renderContestAsCards = () => {
    const { contestList } = this.state;
    return contestList.map(contest => (
      <Card
        key={contest.header}
        color={CARD_COLORS[getRandomInt(0, CARD_COLORS.length - 1)]}
        value={contest.header}
        onClick={(e, { value }) => {
          this.props.history.push(`/leaderboard/${value}`);
        }}
      >
        <Card.Content>
          <Card.Header>{contest.header}</Card.Header>
          <Card.Meta>{contest.meta}</Card.Meta>
          <Card.Description>{contest.description}</Card.Description>
        </Card.Content>
      </Card>
    ));
  };

  render() {
    const { loading, sortField, sortOrder } = this.state;
    if (loading) return <SegmentLoader />;
    return (
      <div>
        <div style={{ display: 'flex', height: '40px' }}>
          <Dropdown
            defaultValue={sortField}
            onChange={(e, { value }) =>
              this.handleChange({ value, fieldType: 'sortField' })
            }
            selection
            options={SORT_CONTESTS_FIELDS}
          />
          <div style={{ paddingLeft: '5px' }}>
            <Form.Field>
              <Radio
                label="Ascending"
                name="radioGroup"
                value="asc"
                checked={sortOrder === 'asc'}
                onChange={(e, { value }) =>
                  this.handleChange({ value, fieldType: 'sortOrder' })
                }
              />
            </Form.Field>
            <Form.Field>
              <Radio
                label="Descending"
                name="radioGroup"
                value="desc"
                checked={sortOrder === 'desc'}
                onChange={(e, { value }) =>
                  this.handleChange({ value, fieldType: 'sortOrder' })
                }
              />
            </Form.Field>
          </div>
        </div>
        <div style={{ marginTop: '10px', height: '88vh', overflowY: 'scroll' }}>
          <Card.Group>{this.renderContestAsCards()}</Card.Group>
        </div>
      </div>
    );
  }
}

ContestsList.propTypes = {};

export default ContestsList;
